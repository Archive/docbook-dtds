#!/bin/bash
# install script for Docbook
# Written by Colin Walters <walters@verbum.org>
# Based heavily on docbook-dtds.spec from Fedora package git
# http://pkgs.fedoraproject.org/gitweb/?p=docbook-dtds.git;a=commit;h=71e6818dc43d29baca61a8d85c63fec146bdc65a

set -e
set -x

sgmlconfdir=${DESTDIR}${sysconfdir}/sgml
mkdir -p ${sgmlconfdir}
sgmldatadir=${DESTDIR}${datadir}/sgml
mkdir -p ${sgmldatadir}

for format in xml sgml; do
    ln -sf ${format}-docbook-${CURRENT_VERSION}.cat ${sgmlconfdir}/${format}-docbook.cat
done

for formatversion in ${VERSIONS}; do	
    format=${formatversion%%/*}
    version=${formatversion#*/}
    instdir=${sgmldatadir}/docbook/${format}-dtd-${version}
    fmt_srcdir=${srcdir}/${formatversion}
    mkdir -p ${instdir}
    case $format in
	sgml) install -m 0644 ${fmt_srcdir}/*.dcl ${instdir};;
	xml) mkdir -p ${instdir}/ent; install -m 0644 ${fmt_srcdir}/ent/* ${instdir}/ent;;
    esac
    install -m 0644 ${fmt_srcdir}/*.dtd ${fmt_srcdir}/*.mod ${instdir}
    install -m 0644 ${fmt_srcdir}/docbook.cat ${instdir}/catalog

    # Empty cache file (for rpm, %ghost)
    touch ${sgmlconfdir}/${format}-docbook-${version}.cat
done
    

