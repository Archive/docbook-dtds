include Makefile.inc

CURRENT_VERSION=4.5

VERSIONS=$(addprefix sgml/,3.0 3.1 4.0 4.1 4.2 4.3 4.4 4.5) $(addprefix xml/,4.1.2 4.2 4.3 4.4 4.5)

bin_programs = docbook-dtds-regenerate-catalog

docbook-dtds-regenerate-catalog: $(srcdir)/docbook-dtds-regenerate-catalog.in
	sed -e "s,@sysconfdir\@,$(sysconfdir)," \
	    -e "s,@datadir\@,$(datadir)," \
	    -e "s,@VERSIONS\@,$(VERSIONS)," \
	    $< > $@.tmp && mv $@.tmp $@

all: $(bin_programs)

install: $(bin_programs)
	CURRENT_VERSION="$(CURRENT_VERSION)" VERSIONS="$(VERSIONS)" DESTDIR="$(DESTDIR)" sysconfdir="$(sysconfdir)" datadir="$(datadir)" srcdir="${srcdir}" ${srcdir}/dtds-makeinstall.sh

	mkdir -p $(DESTDIR)$(bindir)
	for prog in $(bin_programs); do \
		install -m 0755 $$prog $(DESTDIR)$(bindir)/$$prog; \
	done
